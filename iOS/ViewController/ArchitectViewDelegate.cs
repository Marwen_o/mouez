﻿using System;
using Foundation;
using Wikitude.Architect;

namespace LikwidAR.iOS
{
	public partial class ArchitectViewDelegate : WTArchitectViewDelegate
	{

		CustomARViewRendered customARViewRendered;

		public ArchitectViewDelegate(CustomARViewRendered customARViewRendered) : base()
		{
			this.customARViewRendered = customARViewRendered;
		}

		public override void InvokedURL(WTArchitectView architectView, NSUrl url)
		{
			Console.WriteLine("architect view invoked url: " + url);
			(this.customARViewRendered.Element as CustomARView).IsURLInvocked = true;
		}

		public override void DidFinishLoadNavigation(WTArchitectView architectView, WTNavigation navigation)
		{
			Console.WriteLine("architect view loaded navigation: " + navigation.OriginalURL);
		}

		public override void DidFailToLoadNavigation(WTArchitectView architectView, WTNavigation navigation, NSError error)
		{
			Console.WriteLine("architect view failed to load navigation. " + error.LocalizedDescription);
		}
	}
}