﻿using System;
using UIKit;
using Foundation;
using Wikitude.Architect;
using CoreMotion;
using CoreLocation;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LikwidAR.iOS
{
	public class ARViewController : UIViewController
	{
		#region Class Attributes

		protected WTArchitectView arView;
		protected WTNavigation navigation;
		protected ArchitectViewDelegate architectViewDelegate;
		public delegate void Action<WTStartupConfiguration>(WTStartupConfiguration startUpConfig);

		public CustomARViewRendered customARViewRendered;
		public LocationManager locationManager;
		public CLLocation userLocation;
		public List<Drop> drops;
		public string DataAsString;
		public int LocationUpdateCount = 0;

		#endregion

		#region Constructors

		public ARViewController() : base()
		{
			this.locationManager = new LocationManager();
			this.drops = new List<Drop>();
		}

		public ARViewController(CustomARViewRendered customARViewRendered) 
		{
			this.locationManager = new LocationManager();
			this.drops = new List<Drop>();
			this.customARViewRendered = customARViewRendered;
		}

		#endregion

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
			{
				EdgesForExtendedLayout = UIRectEdge.None;
			}

			var requiredFeatures = WTFeatures.Geo;
			var error = new NSError();
			if (WTArchitectView.IsDeviceSupportedForRequiredFeatures(requiredFeatures, out error))
			{
				//Instanciate ArchitectView and set Wikitude SDK Licence Key
				var motion = new CMMotionManager();
				arView = new WTArchitectView(UIScreen.MainScreen.Bounds, motion);
				arView.SetLicenseKey(Constants.WIKITUDE_SDK_KEY);

				//register Architect View Delegate to Architect View
				this.architectViewDelegate = new ArchitectViewDelegate(this.customARViewRendered);
				this.arView.Delegate = architectViewDelegate;

				//Load the AR View
				var	absoluteWorldUrl = NSBundle.MainBundle.BundleUrl.AbsoluteString + "ArchitectWorld" + "/index.html";
				var u = new NSUrl(absoluteWorldUrl);
				arView.LoadArchitectWorldFromURL(u, requiredFeatures);
				View.AddSubview(arView);
			}
			else
			{
				var adErr = new UIAlertView("Unsupported Device", "This device is not capable of running ARchitect Worlds. Requirements are: iOS 5 or higher, iPhone 3GS or higher, iPad 2 or higher. Note: iPod Touch 4th and 5th generation are only supported in WTARMode_IR.", null, "OK", null);
				adErr.Show();
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			if (arView != null)
			{
				arView.Start((startupConfiguration) =>
		   		{
					   startupConfiguration.CaptureDevicePosition = AVFoundation.AVCaptureDevicePosition.Back;
		   		}, (isRunning, error) =>
				   {
			   		if (isRunning)
			   		{
						   Console.WriteLine("Wikitude SDK version " + WTArchitectView.SDKVersion + " is running.");
			   		}
			   		else
			   		{
				   		Console.WriteLine("Unable to start Wikitude SDK. Error: " +  error.LocalizedDescription);
			   		}
		   		});

				this.DataAsString = "[{\"Id\":1,\"Type\":\"Image\",\"Title\":\"SICK TRICK CONTEST\",\"Thumbnail\":\"./sicktrickcontest.png\",\"Emoji\":\"./emojichallenge.png\",\"Latitude\":41.3874638935,\"Longitude\":2.144473999995,\"Altitude\":23},{\"Id\":2,\"Type\":\"Image\",\"Title\":\"SELFIE EVENT\",\"Thumbnail\":\"file:///assets/temp/dropthmbnail2.png\",\"Emoji\":\"file:///assets/temp/emojiselfie.png\",\"Latitude\":41.36746905,\"Longitude\":2.148970499999995,\"Altitude\":27},{\"Id\":3,\"Type\":\"Video\",\"Title\":\"RACE EVENT\",\"Thumbnail\":\"file:///assets/temp/Drop32x32.png\",\"Emoji\":\"file:///assets/temp/emojirace.png\",\"Latitude\":41.3974605,\"Longitude\":2.1689799999995,\"Altitude\":35},{\"Id\":4,\"Type\":\"Image\",\"Title\":\"CHALLENGE SELFIE\",\"Thumbnail\":\"file:///assets/temp/challengeselfie.png\",\"Emoji\":\"file:///assets/temp/emojiselfie.png\",\"Latitude\":41.317405,\"Longitude\":2.188979999995,\"Altitude\":40}]";

				this.drops = JsonConvert.DeserializeObject<List<Drop>>(DataAsString);

				locationManager.StartLocationUpdates();

				locationManager.LocationUpdated += HandleLocationChanged;
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (arView != null)
				arView.Stop();
		}

		public void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
		{
			// Handle foreground updates

			if (this.LocationUpdateCount == 0)
			{
				this.LocationUpdateCount++;

				CLLocation location = e.Location;
				this.userLocation = new CLLocation();
				this.userLocation = location;

				RandomDrops(this.userLocation, ref this.drops);

				this.DataAsString = JsonConvert.SerializeObject(this.drops);

				Console.WriteLine(this.DataAsString);

				var js = "World.loadPoisFromJsonData(" + DataAsString + ")";

				this.arView.CallJavaScript(js);
			}
		}

		public void RandomDrops(CLLocation loc, ref List<Drop> drops) 
		{
			var lat = loc.Coordinate.Latitude;
			var lon = loc.Coordinate.Longitude;
			var alt = loc.Altitude;

			var a = -1;
			for (int i = 0; i < drops.Count; i++) 
			{
				//var rnd = new Random();
				drops[i].Latitude = lat + a * (i + 1) * 0.01;
				drops[i].Longitude = lon + a * (i + 2) * 0.01;
				drops[i].Altitude = alt + a * (i + 3);
				a *= -1;
			}
		}

}
	
}
