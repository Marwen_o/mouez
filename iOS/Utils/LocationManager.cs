﻿using CoreLocation;
using UIKit;
using System;
using Foundation;

namespace LikwidAR.iOS
{
	public class LocationManager
	{
		//location manager instance
		protected CLLocationManager locMgr;

		// event for the location changing
		public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };

		public CLLocationManager LocMgr
		{
			get { return this.locMgr; }
		}

		public LocationManager()
		{
			this.locMgr = new CLLocationManager();
			this.locMgr.PausesLocationUpdatesAutomatically = false;

			//locMgr.RequestAlwaysAuthorization(); // works in background
			locMgr.RequestWhenInUseAuthorization(); // only in foreground
			
			//LocationUpdated += PrintLocation;
		}

		public void StartLocationUpdates() 
		{
			if (CLLocationManager.LocationServicesEnabled)
			{
				//set the desired accuracy, in meters
				LocMgr.DesiredAccuracy = 1;
				LocMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
				{
	  				// fire our custom Location Updated event
	  				LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
				};

				// Start our location updates
				LocMgr.StartUpdatingLocation();

				// Get some output from our manager in case of failure
				LocMgr.Failed += (object sender, NSErrorEventArgs e) =>
				{
					Console.WriteLine(e.Error);
				};
			}
			else
			{
				//Let the user know that they need to enable LocationServices
				Console.WriteLine("Location services not enabled, please enable this in your Settings");
			}
		}

		public void StopLocationUpdates() 
		{
			this.LocMgr.StopUpdatingLocation();
		}
	}
}