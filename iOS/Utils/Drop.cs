﻿using System;
namespace LikwidAR.iOS
{
	public class Drop
	{
		public int Id { get; set;}
		public string Type { get; set;}
		public string Title { get; set; }
		public string Thumbnail { get; set; }
		public string Emoji { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public double Altitude { get; set; }
	}
}
