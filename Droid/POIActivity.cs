﻿using Android.App;
using Android.Content;
using Android.OS;
using Xamarin.Forms;

namespace LikwidAR.Droid
{
	[Activity(Label = "PoiActivity")]
	public class PoiActivity : Activity
	{

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			string id = Intent.GetStringExtra("id");

			MessagingCenter.Send(this, "id", id);
		}
	}
}