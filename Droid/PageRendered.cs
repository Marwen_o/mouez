﻿using LikwidAR;
using LikwidAR.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomARView), typeof(PageRendered))]
namespace LikwidAR.Droid
{
	public class PageRendered : PageRenderer
	{
		public PageRendered()
		{

		}

		protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
		{
			base.OnElementChanged(e);

			// this is a ViewGroup - so should be able to load an AXML file and FindView<>
			Context.StartActivity(typeof(BasicArchitectActivity));
		}

		protected void toIdPage(PoiActivity obj, string id)
		{
			Xamarin.Forms.Application.Current.MainPage = new DropPage(id);
		}
	}
}