﻿using Android.App;
using Android.OS;
using Xamarin.Forms;

namespace LikwidAR.Droid
{
	[Activity(Label = "Arch World", MainLauncher = true)]

	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		App app;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			Xamarin.Forms.Forms.Init(this, bundle);

			app = new App();
			LoadApplication(app);

			MessagingCenter.Subscribe<PoiActivity, string>(
					 this,
					 "id",
					 toIdPage
			);
		}

		private void toIdPage(PoiActivity arg1, string arg2)
		{
			app.toIdPage(arg2);
		}

	}
}


