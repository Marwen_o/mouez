﻿using Xamarin.Forms;

namespace LikwidAR
{
	public partial class DropPage : ContentPage
	{
		public DropPage(string idPoi)
		{
			Label header = new Label
			{
				Text = "PoiData ID = " + idPoi,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			Content = new StackLayout
			{
				Children = {
					header
				}
			};
		}
	}
}
