﻿using Xamarin.Forms;

namespace LikwidAR
{
    public partial class CustomARView : ContentPage
    {
        bool isURLInvocked;

        public bool IsURLInvocked
        {
            get
            {
                return isURLInvocked;
            }
            set
            {
                isURLInvocked = value;
                this.OnPropertyChanged("IsURLInvocked");
                if (IsURLInvocked)
                {
                    System.Diagnostics.Debug.WriteLine("URL Is Invocked ! ");
                    toNextPage("1");
                    System.Diagnostics.Debug.WriteLine("Main Page launched ! ");
                    //PushChallengeView();
                }
            }
        }

        public CustomARView()
        {
            InitializeComponent();
        }

        public void toNextPage(string id)
        {
            Navigation.PushModalAsync(new DropPage(id));
        }

    }
}

